public class Logic1 {

    public static void main(String[] args) {
        soal01(9);
        soal02(9);
        soal03(9);
        soal04(9);
        soal05(9);
        //soal06(9);
        soal07(9);
        soal08(9); // masih salah
        soal09(9);
        soal10(9);

    }

    public static void soal01(int n) {
        int[] deret = new int[n];
        int j = 1;
        for (int i = 0; i < n; i++) {
            deret[i] = j;
            j++;
            System.out.print(deret[i]+"\t");
        }
        System.out.println("\n Soal 1 =======================================");
    }

    public static void soal02(int n) {
        int[] deret = new int[n];
        int j = 1;
        int k = 1;
        for (int i = 0; i < n; i++) {
            if (i %2 == 0){
                deret[i] = j;
                j++;
                System.out.print(deret[i]+"\t");

            }else {

                deret[i] = k*3;
                k++;
                System.out.print(deret[i]+"\t");
            }
        }
        System.out.println("\n Soal 2 =======================================");
    }

    public static void soal03(int n){
        int[] deret = new int[n];

        for (int i = 0; i < n; i++) {
            deret[i] = i*2;

            System.out.print(deret[i]+"\t");
        }
        System.out.println("\n Soal 3 =======================================");
    }

    public static void soal04(int n){
        int[] deret = new int[n];

        for (int i = 0; i < n; i++){
            if (i < 2) {
                deret[i] = 1;
                System.out.print(deret[i]+"\t");
            } else {
                deret[i] = deret[i - 1] + deret[i - 2];
                System.out.print(deret[i]+"\t");
            }
        }
        System.out.println("\n Soal 4 =======================================");
    }

    //soal 5
    public static void soal05(int n){
        int[] deret = new int[n];

        for (int i = 0; i < n; i++){
            if (i < 3) {
                deret[i] = 1;
                System.out.print(deret[i]+"\t");
            } else {
                deret[i] = deret[i - 1] + deret[i - 2] + deret[i-3];
                System.out.print(deret[i]+"\t");
            }
        }
        System.out.println("\n Soal 5 =======================================");
    }

    //soal 6
//    public static void soal06(int n){
//        int[] deret = new int[n];
//        for (int i = 0; i< n; i++){
//            int k = 0;
//            deret[i] = 1;
//            System.out.print(deret[i]+"\t");
//        }
//        System.out.println("\n Soal 6 =======================================");
//    }

    public static void soal07(int n) {
        char k = 'A';
        int[] deret = new int[n];
        for (int i=0; i< deret.length; i++) {
            try{
                char j = (char)(deret[i] = k);
                k++;
                System.out.print(j+"\t");
            } catch(Exception e){
                System.out.println("Error : " + e);
            }

        }
        System.out.println("\n Soal 7 =======================================");
    }

    public static void soal08(int n) {
        int[] deret = new int[n];
        char j = 'A';
        for ( int i=0; i<n; i++) {
            if (i %2 == 0){
                deret[i] = j;

                char k = (char)(deret[i]);
                System.out.print(k+"\t");
            } else {
                deret[i] = i+1;
                System.out.print(deret[i]+"\t");
            }
            j++;
        }
        System.out.println("\n Soal 8 =======================================");
    }

    public static void soal09(int n) {
        int[] deret = new int[n];
        int k = 1;
        for (int i = 0; i < n; i++) {
            deret[i] = k;
            System.out.print(deret[i]+"\t");
            k = deret[i]*3;
        }
        System.out.println("\n Soal 9 =======================================");
    }

    public static void soal10(int n) {
        int[] deret = new int[n];

        for (int i = 0; i < n; i++) {
            double k = (double)(i);
            deret[i] = (int)(Math.pow(k,3));
            System.out.print(deret[i]+"\t");

        }
        System.out.println("\n Soal 10 =======================================");
    }
}
